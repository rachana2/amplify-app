import router from './router'
import Vue from 'vue';
import App from './App.vue';

import Amplify from 'aws-amplify';
import '@aws-amplify/ui-vue';
import aws_config from './aws-exports';

// import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import '@/styles.scss'

Amplify.configure(aws_config);
Vue.config.productionTip = false

new Vue({
    router,
    vuetify,
    render: h => h(App)
}).$mount('#app')
