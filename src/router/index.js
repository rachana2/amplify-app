import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import {loadLocaleI18nAsync} from '@/i18n';

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    redirect: "en"
  },
  {
    path: "/:locale",
    name: "home",
    component: Home,
    meta:{ requireAuth: true }
  },
  {
    path: "/:locale/dashboard",
    name: "dashboard",
    component: () => import("@/views/user/Dashboard.vue"),
    meta:{ requireAuth: true }
  },
  {
    path: "/:locale/account",
    name: "account",
    component: () => import("@/views/user/Account.vue"),
    meta:{ requireAuth: true }
  },
  {
    path: "/:locale/signup",
    name: "signup",
    component: () => import("@/views/auth/Signup.vue")
  },
  {
    path: "/:locale/confirm_signup",
    name: "confirmSignup",
    component: () => import("@/views/auth/ConfirmSignup.vue")
  },
  {
    path: "/:locale/forgot_password",
    name: "forgotPassword",
    component: () => import("@/views/auth/ForgotPassword.vue")
  },
  {
    path: "/:locale/login",
    name: "login",
    component: () => import("@/views/auth/Login.vue")
  }
]

const router = new VueRouter({
  mode: 'history',
  //base: process.env.BASE_URL,
  routes
})


router.beforeEach(async (to, from, next) => {
  // if(to.matched.some(record=> record.meta.requireAuth)){
  //   try{
  //     await Vue.prototype.$Amplify.Auth.currentAuthenticatedUser();
  //     next();
  //   }catch(error){
  //     next({path: "/"});
  //   }
  // }
  if (to.params.locale === from.params.locale) {
    next()
    return
  }
  loadLocaleI18nAsync(to.params.locale).then(() => {
  })
  next()
})

export default router
